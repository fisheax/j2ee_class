/**
 * @FileName:
 * @Package: com.fisheax.spring
 * @author: fisheax
 * @date: 5/20/16
 */
package com.fisheax.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @ClassName: SpringDemo
 * @Description:
 * @author: fisheax
 * @date: 5/20/16
 */
public class SpringDemo
{
	public static void main(String[] args)
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-demo.xml");
		User user = ((User) context.getBean("user"));
		System.out.println(user);
	}
}
