/**
 * @FileName:
 * @Package: com.fisheax.spring
 * @author: fisheax
 * @date: 5/20/16
 */
package com.fisheax.spring;

/**
 * @ClassName: User
 * @Description:
 * @author: fisheax
 * @date: 5/20/16
 */
public class User
{
	private String username;
	private String password;

	@Override
	public String toString()
	{
		return "User{" +
			       "username='" + username + '\'' +
			       ", password='" + password + '\'' +
			       '}';
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
