/**
 * @FileName:
 * @Package: com.fisheax.struts
 * @author: fisheax
 * @date: 5/20/16
 */
package com.fisheax.struts;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @ClassName: LoginController
 * @Description:
 * @author: fisheax
 * @date: 5/20/16
 */
public class LoginController extends ActionSupport
{
	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	private String username;
	private String password;

	public String login()
	{
		if("fish".equals(username) && "pass".equals(password))
			return "success";
		else
			return "error";
	}
}
