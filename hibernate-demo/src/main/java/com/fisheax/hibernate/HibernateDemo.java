/**
 * @FileName:
 * @Package: com.fisheax.hibernate
 * @author: fisheax
 * @date: 5/21/16
 */
package com.fisheax.hibernate;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @ClassName: HibernateDemo
 * @Description:
 * @author: fisheax
 * @date: 5/21/16
 */
public class HibernateDemo
{
	public static void main(String[] args)
	{
		Configuration configuration = new Configuration().configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();

		Query query = session.createQuery("from User");
		System.out.println(query.list());

		session.close();
	}
}
