/**
 * @FileName:
 * @Package: com.fisheax.hibernate
 * @author: fisheax
 * @date: 5/20/16
 */
package com.fisheax.hibernate;

/**
 * @ClassName: User
 * @Description:
 * @author: fisheax
 * @date: 5/20/16
 */
public class User
{
	private int userid;
	private String username;
	private String password;

	@Override
	public String toString()
	{
		return "User{" +
			       "userid=" + userid +
			       ", username='" + username + '\'' +
			       ", password='" + password + '\'' +
			       '}';
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public int getUserid()
	{
		return userid;
	}

	public void setUserid(int userid)
	{
		this.userid = userid;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
