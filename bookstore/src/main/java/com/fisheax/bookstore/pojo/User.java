/**
 * @FileName User.java
 * @Package tk.lx.bookstore.pojo
 * @author muxue
 * @date Apr 6, 2016
 */
package com.fisheax.bookstore.pojo;

/**
 * @ClassName: User
 * @Description
 * @author muxue
 * @date Apr 6, 2016
 */
public class User
{
	private int userid;
	private String username;
	private String password;

	public int getUserid()
	{
		return userid;
	}
	public void setUserid(int userid)
	{
		this.userid = userid;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	@Override
	public String toString()
	{
		return "User [userid=" + userid + ", username=" + username + ", password=" + password + "]";
	}
}
