/**
 * @FileName UserController.java
 * @Package tk.lx.bookstore.controller
 * @author muxue
 * @date Apr 6, 2016
 */
package com.fisheax.bookstore.controller;

import com.fisheax.bookstore.dao.UserDao;
import com.fisheax.bookstore.pojo.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @ClassName: UserController
 * @Description
 * @author muxue
 * @date Apr 6, 2016
 */
public class UserController extends ActionSupport
{
	private static final long serialVersionUID = 1L;

	private UserDao userDao;

	/**
	 * 参数model
	 */
	private User user;

	/**
	 * 用户登录
	 * @return
	 */
	public String login()
	{
		if(userDao.isUserValid(user))
		{
			ActionContext.getContext().getSession().put("user", user);
			return "success";
		}
		else
			return "error";
	}

	/**
	 * 用户登出
	 * @return
	 */
	public String logout()
	{
		ActionContext.getContext().getSession().remove("user");
		return "success";
	}

	/**
	 * 用户注册
	 * @return
	 */
	public String register()
	{
		userDao.saveUser(user);
		return "success";
	}


	public UserDao getUserDao()
	{
		return userDao;
	}

	public void setUserDao(UserDao userDao)
	{
		this.userDao = userDao;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}
}
