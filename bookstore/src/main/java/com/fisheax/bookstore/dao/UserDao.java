/**
 * @FileName UserDao.java
 * @Package tk.lx.bookstore.dao
 * @author muxue
 * @date Apr 13, 2016
 */
package com.fisheax.bookstore.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.fisheax.bookstore.pojo.User;
import org.hibernate.service.spi.SessionFactoryServiceInitiator;

import java.util.List;

/**
 * @ClassName: UserDao
 * @Description
 * @author muxue
 * @date Apr 13, 2016
 */
public class UserDao
{
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	/**
	 * 检测用户是否存在
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean isUserValid(User user)
	{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from User where username=? and password=?");
		query.setString(0, user.getUsername());
		query.setString(1, user.getPassword());
		List<User> users = query.list();
		session.close();

		return users != null && users.size() == 1;
	}


	/**
	 * 保存用户
	 * @param user
	 */
	public void saveUser(User user)
	{
		Session session = sessionFactory.openSession();
		session.save(user);
		session.close();
	}
}
